package com.collegelacite.marvel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptateurMarvel extends BaseAdapter {
    private Context contexte;
    private LayoutInflater inflater;
    private ArrayList<Marvel> avengers;

    public AdaptateurMarvel(Context cont, ArrayList<Marvel> donees) {
        this.contexte = cont;
        this.avengers = donees;
        this.inflater = (LayoutInflater) this.contexte.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return avengers.size();

    }

    @Override
    public Object getItem(int i) {

        return avengers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

@Override
    public View getView (int i, View view, ViewGroup viewGroup) {

        View rowView = inflater.inflate(R.layout.itemlistmarvel, viewGroup, false);

        Marvel marvel= avengers.get(i);


        ImageView iv = rowView.findViewById(R.id.imgview1);
    iv.setImageResource(marvel.getDrawableId());


return  rowView;

}}
