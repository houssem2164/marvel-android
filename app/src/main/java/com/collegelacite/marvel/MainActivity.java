package com.collegelacite.marvel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

private  AdaptateurMarvel adaptateur ;
private int compteur =0;

    private ArrayList<Marvel> avengers;    // source de données

    // Fonction créant et initialisant l'activité
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Créer la source de données de l'adaptateur
        avengers = Marvel.lireFichier(this);
        adaptateur = new AdaptateurMarvel(this, avengers);
        //connecter gridView a l'adaptateur
        final TextView textView1=findViewById(R.id.tv1id);
        final TextView textView2=findViewById(R.id.txtview2);
        final ConstraintLayout layout = findViewById(R.id.layoutId);
        GridView grid = findViewById(R.id.gridview);
        grid.setAdapter(adaptateur);
     for (Marvel m : avengers){
         m.setDévoilé(true);
     }
     final int[] color =new int[] {Color.GREEN,Color.RED};
        MenuItem menuItem = findViewById(R.id.menuid);
grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (textView2.getText().equals(avengers.get(i).getNom())){

       layout.setBackgroundColor(color[0]);

            Toast toast = Toast.makeText(getApplicationContext(), "Nombre de tentative = "+compteur, Toast.LENGTH_LONG);
            toast.show();
        }
        else {layout.setBackgroundColor(color[1]);
            compteur++;}

    }
});

    }

    private int Intervalle(){

        int maximum = avengers.size();
        int minimum = 1;
        int intervalle = maximum - minimum + 1;
        int random = (int)(Math.random() * intervalle) + minimum;
        return random;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.newmenu, menu);
        return true;
    }

        @Override
        public boolean onOptionsItemSelected(MenuItem item)
        {
            final ConstraintLayout layout = findViewById(R.id.layoutId);
            layout.setBackgroundColor(Color.WHITE);
            TextView textView2=findViewById(R.id.txtview2);
            boolean res=false;
            if (item.getItemId()==R.id.menuid){
                textView2.setText(avengers.get(Intervalle()).getNom());
                res=true;

            }
            else{return super.onOptionsItemSelected(item);}
            return res;
        }
}
